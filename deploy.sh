#!/usr/bin/env bash
# PATHS
DOTFILES_DIR=$(readlink -f `dirname $0`)
OHMYZSH_DIR="$HOME/.oh-my-zsh"
OHMYTMUX_DIR="$HOME/.tmux"
PURE_DIR="$HOME/.zsh/pure"
ZSH_SYNTAX_HIGHLIGHTING_DIR="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting"

if [ -z "$1" ]; then
    DEPLOY_TYPE="minimal"
else
    DEPLOY_TYPE="$1"
fi

if [ $DEPLOY_TYPE = "full" ]; then
    echo "Doing a full deploy..."
    # Install oh-my-zsh
    if [ -d "$OHMYZSH_DIR" ]; then
        echo "oh-my-zsh already installed."
    else
        echo "Installing oh-my-zsh..."
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    fi

    # Install oh-my-tmux
    if [ -d "$OHMYTMUX_DIR" ]; then
        echo "oh-my-tmux already installed."
    else
        echo "Installing oh-my-tmux..."
        git clone https://github.com/gpakosz/.tmux.git "$OHMYTMUX_DIR"
        ln -sf "$OHMYTMUX_DIR/.tmux.conf" ~/.tmux.conf
    fi

    # Install pure
    if [ -d "$PURE_DIR" ]; then
        echo "Pure already installed."
    else
        echo "Installing Pure..."
        mkdir -p "$HOME/.zsh"
        git clone https://github.com/sindresorhus/pure.git "$PURE_DIR"
    fi

    # Install zsh-syntax-highlighting
    if [ -d "$ZSH_SYNTAX_HIGHLIGHTING_DIR" ]; then
        echo "zsh-syntax-highlighting already installed."
    else
        echo "Installing zsh-syntax-highlighting..."
        git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "$ZSH_SYNTAX_HIGHLIGHTING_DIR"
    fi
else
    echo "Doing a minimal deploy..."
fi

# Link
echo dotfiles dir = $DOTFILES_DIR
ln -sf $DOTFILES_DIR/vimrc ~/.vimrc
ln -sf $DOTFILES_DIR/zshrc ~/.zshrc
ln -sf $DOTFILES_DIR/tmux.conf ~/.tmux.conf.local
ln -sf $DOTFILES_DIR/ssh_config ~/.ssh/config

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    echo "Running on Linux"
	ln -sf $DOTFILES_DIR/vimrc-linux ~/.vimrc.local
	ln -sf $DOTFILES_DIR/zshrc-linux ~/.zshrc.local

elif [[ "$OSTYPE" == "darwin"* ]]; then
    echo "Running on macOS"
	ln -sf $DOTFILES_DIR/vimrc-macos ~/.vimrc.local
	ln -sf $DOTFILES_DIR/zshrc-macos ~/.zshrc.local

fi

echo Done
