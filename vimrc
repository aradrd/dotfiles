"	VimTex
let g:vimtex_quickfix_ignore_filters = [
            \ 'Underfull',
            \ 'Overfull',
            \]
let g:Tex_IgnoreLevel = 1

" Maximum amount of tabs.
set tabpagemax=100

" Enable secure external .vimrc files.
set exrc
set secure

"	Aliases
command NT NERDTree

"	Sets the amount of spaces in a tab.
set tabstop=4

"	Stupid tabs.
set expandtab

"	Resume editing files at the same line.
set display+=lastline

"	Sets the amount of spaces in autoindent (0 means as much as tabstop).
set shiftwidth=0

"	Takes care of indentation.
set autoindent

"	Shows row numbers.
set relativenumber
set number

"	Set vs to right side.
set splitright

"	Adds a line under the current row.
" set cursorline

"	Starts searching before pressing return.
set incsearch

"	Adds line and character count.
set ruler

"	Enables syntax highlighting.
syntax on

"   Backspace.
set backspace=indent,eol,start

"	Opens lines without entering edit mode.
nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>

"	Should help indentation?
if has("autocmd")
	filetype indent plugin on
endif

"	Installs vim-plug if it's not installed already.
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

"	Plugins babyyyy.
call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-endwise'
Plug 'arcticicestudio/nord-vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'dense-analysis/ale'
Plug 'thinca/vim-localrc'
Plug 'shime/vim-livedown'
Plug 'lervag/vimtex'
Plug 'Chiel92/vim-autoformat'
Plug 'Shirk/vim-gas'
Plug 'PeterRincker/vim-argumentative'
Plug 'justinmk/vim-syntax-extra'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'stevearc/vim-arduino'
call plug#end()

silent! autocmd vimenter * ++nested colorscheme nord
set background=dark

set tabline=%!MyTabLine()

" Syntax Highlighting for Groovy
autocmd BufNewFile,BufRead *.jenkinsfile set syntax=groovy

function MyTabLine()
	let s = ''
	for i in range(tabpagenr('$'))
		" select the highlighting
		if i + 1 == tabpagenr()
			let s .= '%#TabLineSel#'
		else
			let s .= '%#TabLine#'
		endif

		" set the tab page number (for mouse clicks)
		let s .= '%' . (i + 1) . 'T'

		" the label is made by MyTabLabel()
		let s .= ' ' . (i + 1) . ':%{MyTabLabel(' . (i + 1) . ')} '
	endfor

	" after the last tab fill with TabLineFill and reset tab page nr
	let s .= '%#TabLineFill#%T'

	" right-align the label to close the current tab page
	if tabpagenr('$') > 1
		let s .= '%=%#TabLine#%999Xclose'
	endif

	return s
endfunction

function MyTabLabel(n)
	let buflist = tabpagebuflist(a:n)
	let winnr = tabpagewinnr(a:n)
	return bufname(buflist[winnr - 1])
endfunction

source $HOME/.vimrc.local

